
/**
 * 
 * Diberikan function tukarBesarKecil(kalimat) yang menerima satu parameter berupa string.
 * Function akan me-return string tersebut dengan menukar karakter yang uppercase menjadi lowercase, dan sebaliknya.
 * Spasi dan simbol diabaikan.
 * 
 */


function tukarBesarKecil(kalimat) {
    // you can only write your code here!
    let arrKalimat = kalimat.split("");
    let tukar = [];
    for (let i = 0; i < arrKalimat.length; i++) {
        if (arrKalimat[i].charCodeAt() >= 65 && arrKalimat[i].charCodeAt() <= 90) {
            tukar.push(arrKalimat[i].toLowerCase());
        } else if (arrKalimat[i].charCodeAt() >= 97 && arrKalimat[i].charCodeAt() <= 122) {
            tukar.push(arrKalimat[i].toUpperCase());
        } else {
            tukar.push(arrKalimat[i]);
        }
    }
    return tukar.join("");
}
  
// TEST CASES
console.log(tukarBesarKecil('Hello World')); // "hELLO wORLD"
console.log(tukarBesarKecil('I aM aLAY')); // "i Am Alay"
console.log(tukarBesarKecil('My Name is Bond!!')); // "mY nAME IS bOND!!"
console.log(tukarBesarKecil('IT sHOULD bE me')); // "it Should Be ME"
console.log(tukarBesarKecil('001-A-3-5TrdYW')); // "001-a-3-5tRDyw"
